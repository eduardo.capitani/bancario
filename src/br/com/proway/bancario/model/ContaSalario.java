package br.com.proway.bancario.model;

import br.com.proway.bancario.interfaces.Operacao;
import br.com.proway.bancario.utils.Meses;

public class ContaSalario extends Conta implements Operacao {
	
	public static final double PORCENTAGEM_JUROS = 0.5;

	private Double juros;
	private double juroCalculado;
	private Meses meses;

	public Double getJuros() {
		return juros;
	}

	public void setJuros(Double juros) {
		this.juros = juros;
	}

	@Override
	public Double calcularJuros(int mesC, int mesD) {
		if (mesD < mesC){
			juroCalculado = ((mesD - mesC) * PORCENTAGEM_JUROS / 100) * -1;
			return juroCalculado ;
		}else {
			juroCalculado = ((mesD - mesC) * PORCENTAGEM_JUROS / 100);
			return juroCalculado;
		}
		
	}

	public double Saldototal(){
		return super.getSaldo() + juroCalculado;
	}

	@Override
	public String toString() {
		return "Conta Salario: " + super.getAgencia() + " " + super.getNumero() + "-" + getDigitoVerificador() + "\n";
	}

}
