package br.com.proway.bancario.model;

import br.com.proway.bancario.utils.Meses;

public class ContaCorrente extends Conta {

	private Double limite;
	private Meses meses;


	public Double getLimite() {
		return limite;
	}

	public void setLimite(Double limite) {
		this.limite = limite;
	}

	@Override
	public String toString() {
		return "Conta Corrente: " + super.getAgencia() + " "
				+ super.getNumero() + "-" + getDigitoVerificador() + "\n"  + "Conta criada em: " + this.meses.getDescricao() + "\n";
	}
	
}
