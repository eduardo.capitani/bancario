package br.com.proway.bancario.model;

import br.com.proway.bancario.utils.Meses;

import java.util.ArrayList;

public abstract class Conta extends Object {
	
	private int agencia;
	private int numero;
	private Integer digitoVerificador;
	private double saldo;

	private Meses contaCriadaEm;

	private ArrayList<Conta> lista = new ArrayList();

	public ArrayList<Conta> getLista() {
		return lista;
	}

	public void setLista(ArrayList<Conta> lista) {
		this.lista = lista;
	}

	public void adicionarConta(Conta conta) {
		lista.add(conta);
	}

	public Meses getContaCriadaEm() {
		return contaCriadaEm;
	}

	public void setContaCriadaEm(Meses contaCriadaEm) {
		this.contaCriadaEm = contaCriadaEm;
	}

	public int getAgencia() {
		return agencia;
	}

	public void setAgencia(int agencia) {
		this.agencia = agencia;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Integer getDigitoVerificador() {
		return digitoVerificador;
	}

	public void setDigitoVerificador(char digitoVerificador) {
		this.digitoVerificador = digitoVerificador;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public Conta() {
		super();
		System.out.println("Entrou no construtor.");
	}
	
	public Conta(int numero, char digitoVerificador) {
		super();
		this.numero = numero;
		this.digitoVerificador = digitoVerificador;
	}

	public Conta(int agencia, int numero, Integer digitoVerificador, double saldo) {
		super();
		this.agencia = agencia;
		this.numero = numero;
		this.digitoVerificador = digitoVerificador;
		this.saldo = saldo;
	}

}
