package br.com.proway.bancario.interfaces;

import br.com.proway.bancario.utils.Meses;

public interface Operacao {
	
	public Double calcularJuros(Double valor, Meses meses);

}
