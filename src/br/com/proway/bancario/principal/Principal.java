package br.com.proway.bancario.principal;

import br.com.proway.bancario.model.ContaCorrente;
import br.com.proway.bancario.model.ContaSalario;
import br.com.proway.bancario.utils.Meses;

import javax.swing.*;

//commit
public class Principal {

    public static void main(String[] args) {

        Date data = new Date();
        System.out.println("Data Agora: "+data);

        Conta contaCorrente = new ContaCorrente();
        ContaSalario contaSalario = new ContaSalario();
        while (true) {
            String texto = "";
            for (int i = 0; i < contaCorrente.getLista().size(); i++) {
                texto += contaCorrente.getLista().get(i).getAgencia() +
                        contaCorrente.getLista().get(i).getNumero() +
                        contaCorrente.getLista().get(i).getDigitoVerificador()
                        + contaCorrente.getLista().get(i).getSaldo();
            }

            String[] opcoes = {"Conta Corrente", "Conta Salario"};
            int opcao = JOptionPane.showOptionDialog(null, "Escolha seu tipo de conta \n",
                    "Banco frufru ", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                    null, opcoes, opcoes[0]);


            if (opcao == 0) {


                int agenciaCc = Integer.parseInt(JOptionPane.showInputDialog("Informe a agencia: "));
                int numeroCc = Integer.parseInt(JOptionPane.showInputDialog("Informe o numero da conta: "));
                char digitoVerificadorCc = JOptionPane.showInputDialog("Informe o digito verificador: ").charAt(contaCorrente.getDigitoVerificador());
                double limiteCc = Double.parseDouble(JOptionPane.showInputDialog("informe o limite da conta: "));
                double saldoCc = Double.parseDouble(JOptionPane.showInputDialog("informe seu saldo: "));
                contaCorrente.setAgencia(agenciaCc);
                contaCorrente.setDigitoVerificador(digitoVerificadorCc);
                contaCorrente.setNumero(numeroCc);
                contaCorrente.setSaldo(saldoCc);
                JOptionPane.showMessageDialog(null, "Conta corrente: " + contaCorrente);
                contaCorrente.adicionarConta(new ContaCorrente());

            }

            if (opcao == 1) {

                int agenciaCs = Integer.parseInt(JOptionPane.showInputDialog("informe a agencia: "));
                int numeroCs = Integer.parseInt(JOptionPane.showInputDialog("informe o numero da conta: "));
                char digitoVerificadorCs = (JOptionPane.showInputDialog("informe o digito verificador: ")).charAt(contaSalario.getDigitoVerificador());
                double saldoCs = Double.parseDouble(JOptionPane.showInputDialog("informe o saldo: "));
                int mesesC = Integer.parseInt(JOptionPane.showInputDialog("Informe o mes de criacao da conta: "));
                contaSalario.setAgencia(agenciaCs);
                contaSalario.setDigitoVerificador(digitoVerificadorCs);
                contaSalario.setJuros(ContaSalario.PORCENTAGEM_JUROS);
                contaSalario.setNumero(numeroCs);
                contaSalario.setSaldo(saldoCs);
                //contaSalario.setContaCriadaEm(Meses.JUNHO);
                double juros = contaSalario.calcularJuros(mesesC, Meses.JUNHO.getValor());
                JOptionPane.showMessageDialog(null, "Valor juros calculado: R$ " + juros+ "\n" + "saldo:" + contaSalario.Saldototal() +
                        "Conta Salario: " + contaSalario);

		System.out.println("Conta Salario: " + contaSalarioAlfredo);
		
		
	}

}
