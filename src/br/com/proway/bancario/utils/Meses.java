package br.com.proway.bancario.utils;

public enum Meses {
    JANEIRO(1, "JANEIRO"),
    FEVEIRO(2, "FEVEIRO"),
    MARCO(3, "MARCO"),
    ABRIL(4, "ABRIL"),
    MAIO(5, "MAIO"),
    JUNHO(6, "JUNHO"),
    JULHO(7, "JULHO"),
    AGOSTO(8, "AGOSTO"),
    SETEMBRO(9, "SETEMBRO"),
    OUTUBRO(10, "OUTUBRO"),
    NOVEMBRO(11, "NOVEMBRO"),
    DEZEMBRO(12, "DEZEMBRO");
    private final int valor;
    private final String descricao;

    Meses(int valor, String descricao) {
        this.valor = valor;
        this.descricao = descricao;
    }

    public int getValor() {
        return this.valor;
    }

    public String getDescricao() {
        return this.descricao;
    }
}
